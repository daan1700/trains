#include <iostream>
#include "include/data/DataLoader.h"


bool DataLoader::loadTrains() {
    std::ifstream file("/home/massmonstarr/CLionProjects/TrainsPlayground/src/Trains.txt");
    file.exceptions(std::ifstream::eofbit | std::ifstream::failbit);

    if (!file.is_open()) {
        throw std::runtime_error("Unable to open data file");
    }

    std::shared_ptr<Train> train;
    try {
        while (file) {
            file >> train;
            dockedTrains.push_back(train);
        }
    }
    catch (std::ifstream::failure& e) {
        if (file.rdstate() & std::ifstream::eofbit) {
            std::cout << "All trains loaded" << std::endl;
        }
    }

    file.close();
    distributeTrains();
}



bool DataLoader::loadStations() {
    std::ifstream file("/home/massmonstarr/CLionProjects/TrainsPlayground/src/TrainStations.txt");
    file.exceptions(std::ifstream::eofbit | std::ifstream::failbit);

    if (!file.is_open()) {
        throw std::runtime_error("Unable to open data file");
    }

    std::string stationName;
    std::vector<std::shared_ptr<Vehicle>> availableVehicles;
    std::shared_ptr<Vehicle> vehicle;

    // Load all stations and vehicles
    try {
        while (!file.eof()) {
            file >> stationName;
            while (file.peek() != '\n') {
                file >> vehicle;
                auto x = vehicle.get();
                availableVehicles.push_back(vehicle);
            }

            stations.push_back(std::make_shared<Station>(stationName, availableVehicles));
            availableVehicles.clear();
        }
    }
    catch (std::ifstream::failure& e) {
        if (file.rdstate() & std::ifstream::eofbit) {
            std::cout << "All stations loaded" << std::endl;
        }
    }
}

bool DataLoader::loadRoutes() {
    std::ifstream file("/home/massmonstarr/CLionProjects/TrainsPlayground/src/TrainMap.txt");
    file.exceptions(std::ifstream::eofbit | std::ifstream::failbit);

    if (!file.is_open()) {
        throw std::runtime_error("Unable to open data file");
    }

    std::string departureStation, destinationStation;
    int distance;

    try {
        while (file >> departureStation >> destinationStation >> distance) {
            routes.push_back(std::make_shared<Route>(departureStation, destinationStation, distance));
        }
    }
    catch (std::ifstream::failure& e) {
        if (file.rdstate() & std::ifstream::eofbit) {
            std::cout << "All routes loaded" << std::endl;
        }
    }

    file.close();

    distributeRoutes();
}

bool DataLoader::loadData() {
    // Load Stations First
    loadStations();
    // Load routes and add them to appropriate stations
    loadRoutes();
    // Load trains and add them to appropriate stations
    loadTrains();

    //printStations();
}

// Distribute routes to stations
bool DataLoader::distributeRoutes() {
    for (const auto &station : stations) {
        for (const auto &route : routes) {
            if (route.get()->getDepartureStation() == station.get()->getName()) {
                station.get()->addRoute(route);
            }
        }
    }
}

// Print all stations and their information
void DataLoader::printStations() {
    for (const auto &station : stations) {
        std::cout << station.get()->getName() << std::endl;
        std::cout << "Vehicles available at this station:" << std::endl;
        station.get()->printAvailableVehicles();
        station.get()->printDockedTrains();
    }
}

bool DataLoader::distributeTrains() {
    for (const auto &station : stations) {
        for (const auto &train : dockedTrains) {
            if (train.get()->getDestination() == station.get()->getName()) {
                station.get()->dockTrain(train);
            } else if (train.get()->getDeparture() == station.get()->getName()) {
                station.get()->dockTrain(train);
            }
        }
    }
}

const std::vector<std::shared_ptr<Train>> &DataLoader::getDockedTrains() const {
    return dockedTrains;
}

const std::vector<std::shared_ptr<Station>> &DataLoader::getStations() const {
    return stations;
}

std::ifstream &operator>>(std::ifstream &is, std::shared_ptr<Train> &v) {
    int trainID, maxSpeed, departureHour, departureMinute, arrivalHour, arrivalMinute;
    std::string departure, destination, arrivalTime, departureTime;
    std::vector<int> vehicles;

    char separator = ':';

    DataAssembler assembler;

    // 1 GrandCentral Liege-Guillemins 01:16 02:46 167 5 3 3 3
    // ID From To DepartureTime ArrivalTime MaxSpeed Vehicles...

    // Start by reading everything but the vehicles
    is >> trainID >> departure >> destination >> departureHour >> separator >> departureMinute >> arrivalHour >> separator >> arrivalMinute >> maxSpeed;

    while (is.peek() != '\n') {
        int vehicle = 0;
        is >> vehicle;
        vehicles.push_back(vehicle);
    }

    // Set value to new train
    v = assembler.createTrain(trainID, departure, destination, departureHour, departureMinute,
                              arrivalHour, arrivalMinute, maxSpeed, vehicles);

    return is;
}

std::ifstream &operator>>(std::ifstream &is, std::shared_ptr<Vehicle> &v) {
    int id, type, param0, param1 = 0;

    DataAssembler assembler;

    // Ignore whitespace and (
    while (is.peek() == ' ' || is.peek() == '(') {
        is.ignore();
    }

    is >> id >> type >> param0;

    // If second parameter is available, read it.
    if (is.peek() != ')') {
        is >> param1;
    }

    is.get();
    v = assembler.createVehicle(id, type, param0, param1);

    return is;
}
