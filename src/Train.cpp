#include <iostream>
#include "include/Train.h"

int Train::getTrainNumber() const {
    return trainNumber;
}

const std::string &Train::getDeparture() const {
    return departure;
}

const std::string &Train::getDestination() const {
    return destination;
}

Train::Train(int trainNumber, TrainStatus status, const std::string &departure, const std::string &destination,
             const std::shared_ptr<Timer> &arrivalTime, const std::shared_ptr<Timer> &departureTime, int maxSpeed)
        : trainNumber(trainNumber), status(status), departure(departure), destination(destination),
          arrivalTime(arrivalTime), departureTime(departureTime), maxSpeed(maxSpeed) {}

const std::string Train::getDepartureAndArrivalInformation() const {
    return "Train number: " + std::to_string(trainNumber) + ": Departs from " + departure + " at: " + departureTime.get()->getTime() + " Scheduled to arrive at " + destination + " at: " + arrivalTime.get()->getTime();
}

const std::vector<int> &Train::getVehicleList() const {
    return vehicleList;
}

void Train::setVehicleList(const std::vector<int> &vehicleList) {
    Train::vehicleList = vehicleList;
}

const std::shared_ptr<Timer> &Train::getArrivalTime() const {
    return arrivalTime;
}

const std::shared_ptr<Timer> &Train::getDepartureTime() const {
    return departureTime;
}

void Train::addVehicle(std::shared_ptr<Vehicle> vehicle) {
    vehicles.push_back(vehicle);
}

void Train::getTrainInfo() {
    std::string statusString;

    switch (status) {
        case TrainStatus::ASSEMBLED:
            statusString = "ASSEMBLED";
            break;
        case TrainStatus::INCOMPLETE:
            statusString = "INCOMPLETE";
            break;
        case TrainStatus::NOT_ASSEMBLED:
            statusString = "NOT ASSEMBLED";
            break;
        case TrainStatus::ARRIVED:
            statusString = "ARRIVED";
            break;
        case TrainStatus::FINISHED:
            statusString = "FINISHED";
            break;
        case TrainStatus::READY:
            statusString = "READ";
            break;
        case TrainStatus::RUNNING:
            statusString = "RUNNING";
            break;
    }

    std::cout << "This is train number: " << trainNumber << std::endl;
    std::cout << "Following vehicles are attached to this train: " << std::endl;

    for (const auto &vehicle : vehicles) {
        std::cout << "Vehicle: " << vehicle->getTypeString() << std::endl;
    }

    std::cout << "Current train status: " << statusString << std::endl;
}

void Train::setStatus(TrainStatus status) {
    Train::status = status;
}

TrainStatus Train::getStatus() const {
    return status;
}

std::vector<std::shared_ptr<Vehicle>> &Train::getVehicles() {
    return vehicles;
}

const std::string Train::trainInformation() const {
    std::string information = "Train number: " + std::to_string(trainNumber) + "\n" +
                                                                               "Status: " + getStatusString() + "\n" +
                                                                               "Departure station: " + departure + "\n" +
                                                                               "Departure time: " + departureTime->getTime() + "\n" +
                                                                               "Destination station: " + destination + "\n" +
                                                                               "Arrival time: " + arrivalTime->getTime() + "\n" +
                                                                               "Attached Vehicles: " + "\n";

    for (const auto &vehicle : vehicles) {
        information += "Vehicle ID: " + std::to_string(vehicle->getId()) + " Type: " + vehicle->getTypeString() + "\n";
    }

    return information;
}

const std::string Train::getStatusString() const {
    std::string statusString;

    switch (status) {
        case TrainStatus::ASSEMBLED:
            statusString = "ASSEMBLED";
            break;
        case TrainStatus::INCOMPLETE:
            statusString = "INCOMPLETE";
            break;
        case TrainStatus::NOT_ASSEMBLED:
            statusString = "NOT ASSEMBLED";
            break;
        case TrainStatus::ARRIVED:
            statusString = "ARRIVED";
            break;
        case TrainStatus::FINISHED:
            statusString = "FINISHED";
            break;
        case TrainStatus::READY:
            statusString = "READ";
            break;
        case TrainStatus::RUNNING:
            statusString = "RUNNING";
            break;
    }

    return statusString;
}





