/**
 * @file        TrainStatus.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_TRAINSTATUS_H
#define TRAINSPLAYGROUND_TRAINSTATUS_H

/**
 * TrainStatus used to set the status of trains
 */
enum class TrainStatus{
    NOT_ASSEMBLED,
    INCOMPLETE,
    ASSEMBLED,
    READY,
    RUNNING,
    ARRIVED,
    FINISHED
};

#endif //TRAINSPLAYGROUND_TRAINSTATUS_H
