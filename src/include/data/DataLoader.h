/**
 * @file        DataLoader.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_DATALOADER_H
#define TRAINSPLAYGROUND_DATALOADER_H

#include "DataAssembler.h"
#include "../Train.h"
#include "../Route.h"
#include "../EventHandler.h"
#include "../Station.h"
#include <fstream>
#include <map>

class DataLoader {
private:
    std::vector<std::shared_ptr<Train>> dockedTrains;
    std::vector<std::shared_ptr<Station>> stations;
    std::vector<std::shared_ptr<Route>> routes;
    bool loadTrains();
    bool loadStations();
    bool loadRoutes();
    bool distributeRoutes();
    bool distributeTrains();
public:
    /**
     * Loads all data, trains, routes, stations
     * @return True if successful
     */
    bool loadData();

    /**
     * Prints all stations and their belongings
     */
    void printStations();

    /**
     * Gets all docked trains
     * @return All docked trains
     */
    const std::vector<std::shared_ptr<Train>> &getDockedTrains() const;

    /**
     * Gets all stations
     * @return All stations
     */
    const std::vector<std::shared_ptr<Station>> &getStations() const;
};

// Overload ifstream >> operator to properly parse vehicles
std::ifstream &operator>>(std::ifstream & is, std::shared_ptr<Vehicle> &v);

// Overload ifstream >> operator to properly parse trains
std::ifstream &operator>>(std::ifstream & is, std::shared_ptr<Train> &v);

#endif //TRAINSPLAYGROUND_DATALOADER_H
