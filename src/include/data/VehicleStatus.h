/**
 * @file        VehicleStatus.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_VEHICLESTATUS_H
#define TRAINSPLAYGROUND_VEHICLESTATUS_H

/**
 * VehicleStatus used to set the status of vehicles
 */
enum class TrainStatus{
    NOT_ASSEMBLED,
    INCOMPLETE,
    ASSEMBLED,
    READY,
    RUNNING,
    ARRIVED,
    FINISHED
};

#endif //TRAINSPLAYGROUND_VEHICLESTATUS_H
