/**
 * @file        DataAssembler.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/
#ifndef TRAINSPLAYGROUND_DATAASSEMBLER_H
#define TRAINSPLAYGROUND_DATAASSEMBLER_H


#include <memory>
#include "../Train.h"
#include "../Station.h"
#include "../TrainStatus.h"

class DataAssembler {
public:

    /**
     * Assembles a station
     * @param name Station name
     * @param availableVehicles List of available vehicles at the station
     * @return Pointer to a station
     */
    std::shared_ptr<Station> createStation(std::string name, std::vector<std::shared_ptr<Vehicle>> availableVehicles);

    /**
     * Creates a vehicle
     * @param id Vehicle ID
     * @param type Vehicle Type
     * @param param0 Value dependant on vehicle type
     * @param param1 Value dependant on vehicle type
     * @return Pointer to a vehicle
     */
    std::shared_ptr<Vehicle> createVehicle(int id, int type, int param0, int param1);

    /**
     * Creates a train
     * @param number Train number
     * @param departure Departure station
     * @param destination Destination station
     * @param arrivalTime Arrival time
     * @param departureTime Departure time
     * @param maxSpeed Max speed
     * @param vehicles Vehicles attached to the train
     * @return Pointer to a train
     */
    std::shared_ptr<Train> createTrain(int number, std::string departure, std::string destination,
                                       int departureHour, int departureMinute, int arrivalHour, int arrivalMinute, int maxSpeed, std::vector<int> vehicles);
};


#endif //TRAINSPLAYGROUND_DATAASSEMBLER_H
