/**
 * @file        Train.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_TRAIN_H
#define TRAINSPLAYGROUND_TRAIN_H

#include <vector>
#include <memory>
#include <string>
#include "Vehicle.h"
#include "TrainStatus.h"
#include "Timer.h"

class Train {
private:
    int trainNumber;
    std::vector<std::shared_ptr<Vehicle>> vehicles;
    std::vector<int> vehicleList;
    TrainStatus status;
    std::string departure;
    std::string destination;
    std::shared_ptr<Timer> arrivalTime;
    std::shared_ptr<Timer> departureTime;
    int maxSpeed;
public:
    int getTrainNumber() const;

    /**
    * Train constructor
    * @param trainNumber Number of the train
    * @param status Current status, type TrainStatus
    * @param departure Departure station
    * @param destination Destination station
    * @param arrivalTime Arrival time
    * @param departureTime Departure time
    * @param maxSpeed Max speed
    */
    Train(int trainNumber, TrainStatus status, const std::string &departure, const std::string &destination,
          const std::shared_ptr<Timer> &arrivalTime, const std::shared_ptr<Timer> &departureTime, int maxSpeed);

    /**
     * Get the name of the departure station
     * @return Departure station name
     */
    const std::string &getDeparture() const;

    /**
     * Get the name of the destination station
     * @return Destination station name
     */
    const std::string &getDestination() const;

    /**
     * Get information regarding departure and arrival
     * @return String containing information regarding departure and arrival
     */
    const std::string getDepartureAndArrivalInformation() const;

    /**
     * Display train number, state, stations, times and vehicles
     * @return String containing the information
     */
    const std::string trainInformation() const;

    /**
     * Get the arrival time
     * @return Timer containing the arrival hour and minute
     */
    const std::shared_ptr<Timer> &getArrivalTime() const;

    /**
     * Get the departure time
     * @return Timer containing the departure hour and minute
     */
    const std::shared_ptr<Timer> &getDepartureTime() const;

    /**
     * Get a list containing the type of vehicles requested by the train
     * @return List of vehicles types represented by an integer
     */
    const std::vector<int> &getVehicleList() const;

    /**
     * Set the list of vehicles
     * @param vehicleList Container with integers representing the type of the vehicles requested by the train
     */
    void setVehicleList(const std::vector<int> &vehicleList);

    /**
     * Add a vehicle to the train
     * @param vehicle Vehicle to add
     */
    void addVehicle(std::shared_ptr<Vehicle> vehicle);

    /**
     * Displays information regarding the train
     */
    void getTrainInfo();

    /**
     * Set the status of the train
     * @param status Status to be set
     */
    void setStatus(TrainStatus status);

    /**
     * Get the status of the train
     * @return The status of the train
     */
    TrainStatus getStatus() const;

    /**
     * Get the status of the train as a string
     * @return The status of the train as a string
     */
    const std::string getStatusString() const;

    /**
     * Get all vehicles attached to the train
     * @return All vehicles attached to the train
     */
    std::vector<std::shared_ptr<Vehicle>> &getVehicles();
};


#endif //TRAINSPLAYGROUND_TRAIN_H
