/**
 * @file        Station.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_STATION_H
#define TRAINSPLAYGROUND_STATION_H

#include <string>
#include <vector>
#include <memory>
#include "Train.h"
#include "Vehicle.h"
#include "Route.h"


class Station {
private:
    std::string name;
    std::vector<std::shared_ptr<Train>> dockedTrains;
    std::vector<std::shared_ptr<Vehicle>> availableVehicles;
    std::vector<std::shared_ptr<Route>> routes;
public:
    /**
     * Station constructor
     * @param name Station name
     * @param vehicles Currently docked vehicles
     */
    Station(std::string name,std::vector<std::shared_ptr<Vehicle>> vehicles);

    const std::string getName() const;

    /**
     * Dock vehicle to station
     * @param vehicle Vehicle to dock
     */
    void addVehicle(std::shared_ptr<Vehicle> vehicle);

    /**
     * Adds a route to the station
     * @param route Route to add
     */
    void addRoute(std::shared_ptr<Route> route);

    /**
     * Docks a train at the station
     * @param train Train to dock
     */
    void dockTrain(std::shared_ptr<Train> train);

    const std::vector<std::shared_ptr<Route>> &getRoutes() const;

    /**
     * Prints all routes belonging to the station
     */
    void printRoutes();

    /**
     * Prints all currently docked trains
     */
    void printDockedTrains();

    /**
     * Prints all vehicles docked to the station
     */
    void printAvailableVehicles();

    /**
     * Assembles a train
     * @param train Train to be assembled
     */
    void assembleTrain(std::shared_ptr<Train> train);
};


#endif //TRAINSPLAYGROUND_STATION_H
