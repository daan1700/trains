/**
 * @file        Route.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_ROUTE_H
#define TRAINSPLAYGROUND_ROUTE_H

#include <string>

class Route {
private:
    std::string departureStation;
    std::string destinationStation;
    int distance;
public:
    Route(const std::string &departureStation, const std::string &destinationStation, int distance);

    const std::string &getDepartureStation() const;

    const std::string &getDestinationStation() const;

    int getDistance() const;
};


#endif //TRAINSPLAYGROUND_ROUTE_H
