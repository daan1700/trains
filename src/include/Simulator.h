/**
 * @file        Simulator.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_SIMULATOR_H
#define TRAINSPLAYGROUND_SIMULATOR_H


#include "Timer.h"
#include "EventHandler.h"
#include "Station.h"
#include "data/DataLoader.h"

class Simulator {
private:
    std::shared_ptr<Timer> timer;
    int runTime;
    std::shared_ptr<DataLoader> data;
    std::unique_ptr<EventHandler> eventHandler;

    // Setup the eventhandler to handle simulation events
    void loadEventHandler();

public:
    /**
     * Simulator constructor
     * @param timer Simulator timer, hour of minute of start of execution
     * @param runTime Time that simulation will run
     * @param data All data, stations, trains etc. This is loaded automatically by the dataloader
     */
    Simulator(const std::shared_ptr<Timer> &timer, int runTime, const std::shared_ptr<DataLoader> &data);

    /**
     * Runs the simulation
     */
    void run();

    /**
     * Advances the simulator a given amount of minutes
     * @param minutes Minutes to advance the simulator
     */
    void advanceSimulator(int minutes);

    /**
     * Get the current time
     * @return The current simulation time
     */
    const std::string getCurrentTime() const;
};


#endif //TRAINSPLAYGROUND_SIMULATOR_H
