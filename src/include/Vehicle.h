/**
 * @file        Vehicle.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_VEHICLE_H
#define TRAINSPLAYGROUND_VEHICLE_H

#include <string>

/**
 * @class Vehicle
 * Vehicle super class representing the base vehicle type
 */
class Vehicle {
private:
    int id;
    int type;

public:
    /**
     * Construct a vehicle with a given ID and Type
     * @param id ID of vehicle
     * @param type Type of vehicle
     */
    Vehicle(int id, int type);

    /**
     * Returns the ID of the vehicle
     * @return ID of the vehicle
     */
    int getId() const;

    /**
     * Returns the type of the vehicle
     * @return The type of the vehicle
     */
    int getType() const;

    /**
     * Returns the type of the vehicle as a string
     * @return Type of vehicle as a string
     */
    const std::string getTypeString() const;
};

/**
 * @class CoachCar
 * Child of Vehicle, with chairs and possibility of wifi
 */
class CoachCar: public Vehicle {
private:
    int numberOfChairs;
    bool availableWifi;
public:
    /**
     * Constructs a coach car
     * @param id ID of vehicle
     * @param type Type of vehicle
     * @param numberOfChairs Number of chairs in vehicle
     * @param availableWifi Wifi available?
     */
    CoachCar(int id, int type, int numberOfChairs, bool availableWifi);
};

/**
 * @class SleepingCar
 * Child of vehicle, with beds.
 */
class SleepingCar: public Vehicle {
private:
    int numberOfBeds;
public:
    /**
     * Constructs a Sleeping car
     * @param id ID of vehicle
     * @param type Type of vehicle
     * @param numberOfBeds Number of beds in vehicle
     */
    SleepingCar(int id, int type, int numberOfBeds);
};

/**
 * @class OpenFreightCar
 * Child of vehicle, with cargo capacity
 */
class OpenFreightCar: public Vehicle {
private:
    int cargoCapacity;
    int floorArea;
public:
    /**
     * Constructs an open freight car
     * @param id ID of vehicle
     * @param type Type of vehicle
     * @param cargoCapacity Cargo capacity of vehicle
     * @param floorArea Floor area of vehicle
     */
    OpenFreightCar(int id, int type, int cargoCapacity, int floorArea);
};

/**
 * @class CoveredFreightCar
 * Child of vehicle, with cargo capacity
 */
class CoveredFreightCar: public Vehicle {
private:
    int cargoCapacity;
public:
    /**
    * Constructs an open freight car
    * @param id ID of vehicle
    * @param type Type of vehicle
    * @param cargoCapacity Cargo capacity of vehicle
    */
    CoveredFreightCar(int id, int type, int cargoCapacity);
};

/**
 * @class ElectricEngine
 * Child of vehicle, with a max speed and power.
 */
class ElectricEngine: public Vehicle {
private:
int maxSpeed;
int power;
public:
    /**
     * Constructs an electric engine
     * @param id ID of vehicle
     * @param type Type of vehicle
     * @param maxSpeed Max speed of vehicle
     * @param power Power of vehicle
     */
    ElectricEngine(int id, int type, int maxSpeed, int power);
};

/**
 * @class DieselEngine
 * Child of vehicle, with a max speed and fuel consumption.
 */
class DieselEngine: public Vehicle {
private:
int maxSpeed;
int fuelConsumption;
public:
    /**
    * Constructs a diesel engine
    * @param id ID of vehicle
    * @param type Type of vehicle
    * @param maxSpeed Max speed of vehicle
    * @param fuelConsumption Fuel consumption of vehicle.
    */
    DieselEngine(int id, int type, int maxSpeed, int fuelConsumption);
};

#endif //TRAINSPLAYGROUND_VEHICLE_H
