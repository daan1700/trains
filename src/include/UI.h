/**
 * @file        UI.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_UI_H
#define TRAINSPLAYGROUND_UI_H


#include "Timer.h"
#include "data/DataLoader.h"
#include "Simulator.h"
#include <string>
#include <memory>

class UI {
private:
    std::shared_ptr<DataLoader> data;
    std::unique_ptr<Simulator> simulator;
public:
    explicit UI(const std::shared_ptr<DataLoader> &data);

    /**
     * Runs simulation from start to finish
     */
    void runSimulation();

    /**
     * Advances simulation 10 minutes at a time
     */
    void advanceSimulation();
    /**
     * Display trains with train numbers, their stations and times
     */
    void displayTimeTable();

    /**
     * Display train number, state, stations, times and vehicles
     * @param trainNumber Number of the train to be inspected
     */
    void displayTrainInfo(int trainNumber);

    /**
     * Display trains at the station with states and vehicles and available vehicles at the station
     * @param station Station to be inspected
     */
    void displayStationInfo(std::string station);

    /**
    * Get the current time
    * @return The current simulation time
    */
    const std::string getCurrentTime() const;
};


#endif //TRAINSPLAYGROUND_UI_H
