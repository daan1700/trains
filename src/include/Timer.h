/**
 * @file        Timer.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_TIMER_H
#define TRAINSPLAYGROUND_TIMER_H

#include <string>

class Timer {
private:
    int hour;
    int minute;
public:
    Timer(int hour, int minute);
    const std::string getTime() const;

    /**
     * Adds given amount to the hour
     * @param amount Amount to add
     */
    const void incrementHour(int amount);

    /**
     * Adds given amount to the minutes
     * @param amount Amount to add
     */
    const void incrementMinute(int amount);

    /**
     * Get the current hour
     * @return The current hour
     */
    int getHour() const;

    /**
     * Get the current minute
     * @return The current minute
     */
    int getMinute() const;
};


#endif //TRAINSPLAYGROUND_TIMER_H
