/**
 * @file        EventHandler.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_EVENTHANDLER_H
#define TRAINSPLAYGROUND_EVENTHANDLER_H


#include <memory>
#include "Train.h"
#include "Station.h"

class EventHandler {
private:
    std::vector<std::shared_ptr<Train>> awaitingAssembly;
    std::vector<std::shared_ptr<Train>> incomplete;
    std::vector<std::shared_ptr<Train>> assembled;
    std::vector<std::shared_ptr<Train>> ready;
    std::vector<std::shared_ptr<Train>> running;
    std::vector<std::shared_ptr<Train>> arrived;
    std::vector<std::shared_ptr<Station>> stations;
    std::vector<int> successfulArrivals;

    // Current Simulation time
    int currentHour;
    int currentMinute;

    // True if assembly and departure should continue, false if not
    bool continueAssembly;

    // Assemble trains
    void assemblyEvent();

    // Launches trains
    void departureEvent();

    // Train arrival
    void arrivalEvent();

    // Deconstructs train vehicles
    void deconstructionEvent();

    void reOrganize();

public:
    EventHandler();

    /**
     * Adds train to assembly line
     * @param train Train to be assembled
     * @return True if successful
     */
    bool addTrain(std::shared_ptr<Train> train);


    void setAwaitingAssembly(const std::vector<std::shared_ptr<Train>> &awaitingAssembly);

    /**
     * Executes events at the given hour and minute
     * @param currentHour Current hour of execution
     * @param currentMinute Current minute of execution
     */
    void executeEvents(int currentHour, int currentMinute);

    /**
     * Set all available stations, this is used to access vehicles and assemble trains
     * @param stations All available stations
     */
    void setStations(const std::vector<std::shared_ptr<Station>> &stations);

    /**
     * Print the status of all trains
     */
    void printTrainStatus();

    /**
     * Print the status of all stations
     */
    void printStationStatus();

    /**
     * Are trains currently running?
     * @return True if there are trains running, false if not
     */
    bool runningTrains();

    /**
     * Prints all running trains
     */
    void listRunningTrains();

    /**
     * Get the current time
     * @return The current simulation time
     */
    const std::string getCurrentTime() const;
};


#endif //TRAINSPLAYGROUND_EVENTHANDLER_H
