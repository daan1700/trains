#include <iostream>
#include "include/UI.h"
#include "exception/TrainNotFoundException.h"
#include "exception/StationNotFoundException.h"

void UI::displayTimeTable() {
    auto trains = data->getDockedTrains();

    for (const auto &train : trains) {
        std::cout << train->getDepartureAndArrivalInformation() << std::endl;
    }
}

UI::UI(const std::shared_ptr<DataLoader> &data) : data(data) {
    auto timer = std::make_shared<Timer>(0, 0);
    int runTime = 24;
    simulator = std::make_unique<Simulator>(timer, runTime, data);
}

void UI::displayTrainInfo(int trainNumber) {
    auto trains = data->getDockedTrains();

    for (const auto &train : trains) {
        if (train->getTrainNumber() == trainNumber) {
            std::cout << train->trainInformation() << std::endl;
            return;
        }
    }


    throw TrainNotFoundException();
}

void UI::displayStationInfo(std::string station) {
    auto stations = data->getStations();

    for (const auto &currentStation : stations) {
        if (currentStation->getName() == station) {
            currentStation->printDockedTrains();
            currentStation->printAvailableVehicles();
            return;
        }
    }

    throw StationNotFoundException();
}

void UI::advanceSimulation() {
    simulator->advanceSimulator(10);
}

void UI::runSimulation() {
    simulator->run();
}

const std::string UI::getCurrentTime() const {
    return simulator->getCurrentTime();
}
