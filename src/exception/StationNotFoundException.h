/**
 * @file        StationNotFoundException.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_STATIONNOTFOUNDEXCEPTION_H
#define TRAINSPLAYGROUND_STATIONNOTFOUNDEXCEPTION_H

#include <exception>

/**
 * @class StationNotFoundException
 * Exception thrown when a station is not found
 */
class StationNotFoundException : public std::exception {
    const char *what() const noexcept override {
        return "Station not found!";
    }
};

#endif //TRAINSPLAYGROUND_STATIONNOTFOUNDEXCEPTION_H
