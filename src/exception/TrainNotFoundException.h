/**
 * @file        TrainNotFoundException.h
 * @author      Daniel Anbarestani
 * @date        October 2017
 * @version     0.1
*/

#ifndef TRAINSPLAYGROUND_TRAINNOTFOUNDEXCEPTION_H
#define TRAINSPLAYGROUND_TRAINNOTFOUNDEXCEPTION_H

#include <exception>

/**
 * @class TrainNotFoundException
 * Exception thrown when a train is not found
 */
class TrainNotFoundException : public std::exception {
public:
    const char *what() const noexcept override {
        return "Train not found!";
    }
};

#endif //TRAINSPLAYGROUND_TRAINNOTFOUNDEXCEPTION_H
