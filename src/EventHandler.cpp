#include <iostream>
#include "include/EventHandler.h"

bool EventHandler::addTrain(std::shared_ptr<Train> train) {
    awaitingAssembly.push_back(train);
    return true;
}


void EventHandler::assemblyEvent() {

    // Set a new timer based on sim timer
    Timer timer(currentHour, currentMinute);

    // Assemble trains 30 minutes before launch
    timer.incrementMinute(30);

    for (const auto &train : awaitingAssembly) {
        if (train->getDepartureTime()->getHour() == timer.getHour() && train->getDepartureTime()->getMinute() == timer.getMinute()) {

            // If they depart in 30 minutes, assemble
            for (const auto &station : stations) {
                if (train->getDeparture() == station->getName()) {
                    std::cout << getCurrentTime() << " Train number: " << train->getTrainNumber() << " is being assembled" << std::endl;
                    station->assembleTrain(train);
                }
            }
        }
    }

    reOrganize();
}

void EventHandler::departureEvent() {
    // Set a new timer based on sim timer
    Timer timer(currentHour, currentMinute);

    // Set train to READY 10 minutes before launch
    timer.incrementMinute(10);

    // Find all assembled trains departing in 10 minutes
    for (int i = 0; i < assembled.size(); i++) {
        auto currentTrain = assembled.at(i).get();
        if (currentTrain->getDepartureTime()->getHour() == timer.getHour() && currentTrain->getDepartureTime()->getMinute() == timer.getMinute()) {

            // If they depart in 10 minutes, set READY
            currentTrain->setStatus(TrainStatus::READY);

            // Change queue
            ready.push_back(static_cast<std::shared_ptr<Train> &&>(assembled.at(i)));
            std::cout << getCurrentTime() << " Train number: " << currentTrain->getTrainNumber() << " is ready for departure" << std::endl;

            // Remove from assembled que
            assembled.erase(assembled.begin() + i);
        }
    }

    // Set a new timer based on sim timer
    Timer departure(currentHour, currentMinute);

    // Depart the trains that are ready to depart
    for (int i = 0; i < ready.size(); i++) {
        auto currentTrain = ready.at(i).get();
        if (currentTrain->getDepartureTime()->getHour() == departure.getHour() && currentTrain->getDepartureTime()->getMinute() == departure.getMinute()) {

            // If they depart, set RUNNING
            currentTrain->setStatus(TrainStatus::RUNNING);

            // Change queue
            running.push_back(static_cast<std::shared_ptr<Train> &&>(ready.at(i)));
            std::cout << getCurrentTime() << " Train number: " << currentTrain->getTrainNumber() << " is departing " << currentTrain->getDeparture() << std::endl;

            // Remove from assembled que
            ready.erase(ready.begin() + i);
        }
    }
}

void EventHandler::arrivalEvent() {
    // Set a new timer based on sim timer
    Timer timer(currentHour, currentMinute);

    // Find all running trains
    for (int i = 0; i < running.size(); i++) {
        auto currentTrain = running.at(i).get();
        if (currentTrain->getArrivalTime()->getHour() == timer.getHour() && currentTrain->getArrivalTime()->getMinute() == timer.getMinute()) {

            // If they have arrived, set ARRIVED
            currentTrain->setStatus(TrainStatus::ARRIVED);

            // Change queue
            arrived.push_back(static_cast<std::shared_ptr<Train> &&>(running.at(i)));
            std::cout << getCurrentTime() << " Train number: " << currentTrain->getTrainNumber() << " is has arrived at " << currentTrain->getDestination() << std::endl;

            successfulArrivals.push_back(currentTrain->getTrainNumber());

            // Remove from running que
            running.erase(running.begin() + i);
        }
    }
}

void EventHandler::deconstructionEvent() {
    // Set a new timer based on sim timer
    Timer timer(currentHour, currentMinute);

    // See if any trains are ready to be deconstructed
    for (int i = 0; i < arrived.size(); i++) {
        auto currentTrain = arrived.at(i).get();
        Timer trainTimer(currentTrain->getArrivalTime()->getHour(), currentTrain->getArrivalTime()->getMinute());
        trainTimer.incrementMinute(20);

        // If train is ready to be deconstructed, deconstruct it
        if (trainTimer.getHour() == timer.getHour() && trainTimer.getMinute() == timer.getMinute()) {
            std::cout << getCurrentTime() << " Train number: " << currentTrain->getTrainNumber() << " is has is being deconstructed at " << currentTrain->getDestination() << std::endl;
            // Get the right station
            for (const auto &station : stations) {
                if (station->getName() == currentTrain->getDestination()) {
                    for (const auto &i : currentTrain->getVehicles()) {
                        station->addVehicle(i);
                    }
                }
            }

            // Clear the train vehicle list
            currentTrain->getVehicles().clear();

            // Remove from arrived que
            arrived.erase(arrived.begin() + i);
        }
    }
}

void EventHandler::setAwaitingAssembly(const std::vector<std::shared_ptr<Train>> &awaitingAssembly) {
    EventHandler::awaitingAssembly = awaitingAssembly;
}

void EventHandler::executeEvents(int currentHour, int currentMinute) {
    this->currentHour = currentHour;
    this->currentMinute = currentMinute;

    // Stop assembly after 24 hours
    if (currentHour == 24) {
        continueAssembly = false;
    }

    // Allow assembly and departure at a given time
    if (continueAssembly) {
        assemblyEvent();
        departureEvent();
    }

    // Continue until all running trains have arrived and been deconstructed
    arrivalEvent();
    deconstructionEvent();

    //printTrainStatus();
    //printStationStatus();
}

void EventHandler::printTrainStatus() {
    std::cout << "Awaiting Assembly:" << std::endl;
    for (const auto &train : awaitingAssembly) {
        std::cout << "Train nr: " << train->getTrainNumber() << ", ";
    }

    std::cout << std::endl;

    std::cout << "Incomplete:" << std::endl;
    for (const auto &train : incomplete) {
        std::cout << "Train nr: " << train->getTrainNumber() << ", ";
    }

    std::cout << std::endl;

    std::cout << "Assembled:" << std::endl;
    for (const auto &train : assembled) {
        std::cout << "Train nr: " << train->getTrainNumber() << ", ";
    }

    std::cout << std::endl;

    std::cout << "Ready:" << std::endl;
    for (const auto &train : ready) {
        std::cout << "Train nr: " << train->getTrainNumber() << ", ";
    }

    std::cout << std::endl;

    std::cout << "Running:" << std::endl;
    for (const auto &train : running) {
        std::cout << "Train nr: " << train->getTrainNumber() << ", ";
    }

    std::cout << std::endl;

    std::cout << "Arrived:" << std::endl;
    for (const auto &train : successfulArrivals) {
        std::cout << "Train nr: " << train << ", ";
    }

    std::cout << std::endl;
}

void EventHandler::setStations(const std::vector<std::shared_ptr<Station>> &stations) {
    EventHandler::stations = stations;
}

void EventHandler::reOrganize() {

    // If the train is assembled, move it to assembled que, else if it's incomplete, set move it to incomplete que
    for (int i = 0; i < awaitingAssembly.size(); i++) {
        if (awaitingAssembly.at(i)->getStatus() == TrainStatus::ASSEMBLED) {
            assembled.push_back(static_cast<std::shared_ptr<Train> &&>(awaitingAssembly.at(i)));
            awaitingAssembly.erase(awaitingAssembly.begin() + i);
        } else if (awaitingAssembly.at(i)->getStatus() == TrainStatus::INCOMPLETE) {
            incomplete.push_back(static_cast<std::shared_ptr<Train> &&>(awaitingAssembly.at(i)));
            awaitingAssembly.erase(awaitingAssembly.begin() + i);
        }
    }
}

void EventHandler::printStationStatus() {
    for (const auto &station : stations) {
        std::cout << "Station " << station->getName() << " has the following vehicles docked:" << std::endl;
        station->printAvailableVehicles();
    }
}

bool EventHandler::runningTrains() {
    for (const auto &train : running) {
        return true;
    }

    return false;
}

void EventHandler::listRunningTrains() {
    std::cout << "Listing running trains: " << std::endl;
    for (const auto &train : running) {
        std::cout << "Train number: " << train->getTrainNumber() << " is " << train->getStatusString() << std::endl;
    }
}

EventHandler::EventHandler() : continueAssembly(true){

}

const std::string EventHandler::getCurrentTime() const {
    std::string timeString;
    if (currentHour < 10 && currentMinute < 10) {
        timeString = "0" + std::to_string(currentHour) + ":" + "0" + std::to_string(currentMinute);
    } else if (currentHour < 10) {
        timeString = "0" + std::to_string(currentHour) + ":" + std::to_string(currentMinute);
    } else if (currentMinute < 10) {
        timeString = std::to_string(currentHour) + ":" + "0" + std::to_string(currentMinute);
    } else {
        timeString = std::to_string(currentHour) + ":" + std::to_string(currentMinute);
    }

    return timeString;
}

