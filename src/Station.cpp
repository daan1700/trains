#include <utility>
#include <iostream>

#include "include/Station.h"

const std::string Station::getName() const {
    return name;
}

// Docks train to station
void Station::dockTrain(std::shared_ptr<Train> train) {
    dockedTrains.push_back(train);
}


void Station::addVehicle(std::shared_ptr<Vehicle> vehicle) {
    availableVehicles.push_back(vehicle);
}

Station::Station(std::string name, std::vector<std::shared_ptr<Vehicle>> vehicles) : name(std::move(name)), availableVehicles(
        std::move(vehicles)){
}

const std::vector<std::shared_ptr<Route>> &Station::getRoutes() const {
    return routes;
}

void Station::addRoute(std::shared_ptr<Route> route) {
    routes.push_back(route);
}

void Station::printRoutes() {
    std::cout << "Routes currently assigned to " << name << ": \n";
    for (const auto &route : routes) {
        std::cout << route.get()->getDepartureStation() << " to " << route.get()->getDestinationStation() << " with the distance " << route.get()->getDistance() << "\n";
    }
}

void Station::printAvailableVehicles() {
    for (const auto &vehicle : availableVehicles) {
        std::cout << "ID: " << vehicle.get()->getId() << " Type: " << vehicle.get()->getTypeString() << std::endl;
    }
}

void Station::printDockedTrains() {
    std::cout << "Trains currently docked to " << name << ": \n";
    for (const auto &train : dockedTrains) {
        train->getTrainInfo();
    }
}

void Station::assembleTrain(std::shared_ptr<Train> train) {
    int vehiclesAttached = 0;

    // Loop through each trains vehicle list, represented by integers
    for (int i : train->getVehicleList()) {
            // Loop through every available vehicle
            for (int y = 0; y < availableVehicles.size(); y++) {
                // If the vehicle matches the one the train seeks, add it to the train and remove it from the station
                if (availableVehicles.at(static_cast<unsigned long>(y))->getType() == i) {
                    train->addVehicle(availableVehicles.at(static_cast<unsigned long>(y)));
                    availableVehicles.erase(availableVehicles.begin() + y);
                    vehiclesAttached++;
                    break; // Break the vehicle loop once it has been added
                }
            }
        }

    // If the required number of vehicles have been added, the train is assembled, otherwise, it is incomplete
    if (vehiclesAttached == train->getVehicleList().size()) {
        train->setStatus(TrainStatus::ASSEMBLED);
    } else {
        train->setStatus(TrainStatus::INCOMPLETE);
    }
}


