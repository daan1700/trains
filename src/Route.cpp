#include "include/Route.h"

Route::Route(const std::string &departureStation, const std::string &destinationStation, int distance)
        : departureStation(departureStation), destinationStation(destinationStation), distance(distance) {}

const std::string &Route::getDepartureStation() const {
    return departureStation;
}

const std::string &Route::getDestinationStation() const {
    return destinationStation;
}

int Route::getDistance() const {
    return distance;
}
