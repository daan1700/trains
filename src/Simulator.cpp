#include <iostream>
#include "include/Simulator.h"

void Simulator::run() {
    int i = 1;
    while (timer->getHour() < runTime || eventHandler->runningTrains()) {
        timer->incrementMinute(1);
        eventHandler->executeEvents(timer->getHour(), timer->getMinute());
    }

    eventHandler->printTrainStatus();
}

Simulator::Simulator(const std::shared_ptr<Timer> &timer, int runTime, const std::shared_ptr<DataLoader> &data) : timer(
        timer), runTime(runTime), data(data), eventHandler(std::make_unique<EventHandler>()) {
    loadEventHandler();
}

void Simulator::loadEventHandler() {
    for (const auto &train : data->getDockedTrains()) {
        eventHandler->addTrain(train);
    }

    eventHandler->setStations(data->getStations());
}

void Simulator::advanceSimulator(int minutes) {
    if (timer->getHour() < runTime || eventHandler->runningTrains()) {
        while (minutes > 0) {
            timer->incrementMinute(1);
            eventHandler->executeEvents(timer->getHour(), timer->getMinute());
            minutes--;
        }
    }

    eventHandler->printTrainStatus();
}

const std::string Simulator::getCurrentTime() const {
    return eventHandler->getCurrentTime();
}

