#include "include/Timer.h"

Timer::Timer(int hour, int minute) : hour(hour), minute(minute) {}

const std::string Timer::getTime() const {
    return std::to_string(hour) + ":" + std::to_string(minute);
}

const void Timer::incrementHour(int amount) {
    // Add amount to hour, this may go over 24
    hour += amount;

    // Adjust if hour has gone over 24
    if (hour > 24) {
        // Calculate how much it is over 24
        int overflow = hour - 24;

        // Reset hour to 0 and add the overflow to it
        hour = 0;

        incrementHour(overflow);
    }
}

const void Timer::incrementMinute(int amount) {
    minute += amount;

    if (minute >= 60) {
        int overflow = minute - 60;

        minute = 0;

        incrementHour(1);
        incrementMinute(overflow);
    }
}

int Timer::getHour() const {
    return hour;
}

int Timer::getMinute() const {
    return minute;
}
