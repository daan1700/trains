#include <iostream>
#include <vector>
#include <memory>
#include <string>
#include <fstream>
#include "include/Train.h"
#include "include/data/DataLoader.h"
#include "include/Simulator.h"
#include "include/UI.h"


int getUserMenuChoice();

int getTrainNumber();

std::string getStationName();

using namespace std;

void displayMenu(std::unique_ptr<UI>& ui)
{
    bool running = true;

    std::cout << "Welcome to train simulator v1.0" << std::endl;

    while (running)
    {
        std::cout << "=========================Menu=========================\n";
        std::cout << "Current time: " << ui->getCurrentTime() << std::endl;
        std::cout << "======================================================\n";
        std::cout << "1: Run entire simulation \n";
        std::cout << "2: Advance simulation 10 minutes \n";
        std::cout << "3: Display time table \n";
        std::cout << "4: Display specific train information \n";
        std::cout << "5: Display specific station information \n";;
        std::cout << "6: Quit \n";
        std::cout << "======================================================\n";


        switch (getUserMenuChoice())
        {
            case 1:
                ui->runSimulation();
                break;
            case 2:
                ui->advanceSimulation();
                break;
            case 3:
                ui->displayTimeTable();
                break;
            case 4:
                try {
                    ui->displayTrainInfo(getTrainNumber());
                } catch (exception& e) {
                    std::cout << e.what() << std::endl;
                }
                break;
            case 5:
                try {
                    ui->displayStationInfo(getStationName());
                } catch (exception& e) {
                    std::cout << e.what() << std::endl;
                }
                break;
            case 6:
                running = false;
                break;
        }
    }
}

string getStationName() {
    std::cout << "Enter station name: ";
    std::string stationName;

    while (!std::cin.bad())
    {
        std::cin >> stationName;

        if (std::cin.good())
        {
            break;
        }
        else if (std::cin.fail())
        {
            std::cin.clear();
            std::string badToken;
            std::cin >> badToken;
            std::cerr << "Bad input: " << badToken << std::endl;
        }
    }

    return stationName;
}

int getTrainNumber() {
    std::cout << "Enter train number: ";
    int trainNumber = 0;

    while (!std::cin.bad())
    {
        std::cin >> trainNumber;

        if (std::cin.good())
        {
            break;
        }
        else if (std::cin.fail())
        {
            std::cin.clear();
            std::string badToken;
            std::cin >> badToken;
            std::cerr << "Bad input: " << badToken << std::endl;
        }
    }

    return trainNumber;
}

int getUserMenuChoice() {

    int userChoice;
    std::cout << "Choice: ";

    while (!std::cin.bad())
    {
        std::cin >> userChoice;

        if (std::cin.good())
        {
            switch (userChoice)
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                    return userChoice;
                default:
                    std::cout << "Unable to perform action. \n";
            }
        }
        else if (std::cin.fail())
        {
            std::cin.clear();
            std::string badToken;
            std::cin >> badToken;
            std::cerr << "Bad input: " << badToken << std::endl;
        }
    }
}

int main() {
    auto loader = make_shared<DataLoader>();
    loader->loadData();

    //auto timer = make_shared<Timer>(0,0);
    //Simulator simulator(timer, 24, loader);

    //simulator.run();

    auto ui = make_unique<UI>(loader);
    displayMenu(ui);

}