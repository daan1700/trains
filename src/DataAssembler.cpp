#include "include/data/DataAssembler.h"

std::shared_ptr<Vehicle> DataAssembler::createVehicle(int id, int type, int param0, int param1) {
    /*
     * 0: CoachCar
     * 1: SleepingCar
     * 2: OpenFreightCar
     * 3: CoveredFreightCar
     * 4: ElectricEngine
     * 5: DieselEngine
     */

    switch (type) {
        case 0:
            // Param0 = numberOfChairs
            // Param1 = availableWifi
            return std::make_shared<CoachCar>(id, type, param0, param1);
        case 1:
            // Param0 = numberOfBeds
            return std::make_shared<SleepingCar>(id, type, param0);
        case 2:
            // Param0 = cargoCapacity
            // Param1 = floorArea
            return std::make_shared<OpenFreightCar>(id, type, param0, param1);
        case 3:
            // Param0 = cargoCapacity
            return std::make_shared<CoveredFreightCar>(id, type, param0);
        case 4:
            // Param0 = maxSpeed
            // Param1 = power
            return std::make_shared<ElectricEngine>(id, type, param0, param1);
        case 5:
            // Param0 = maxSpeed
            // Param1 = fuelConsumption
            return std::make_shared<DieselEngine>(id, type, param0, param1);
        default:break;
    }
}

std::shared_ptr<Station>
DataAssembler::createStation(std::string name, std::vector<std::shared_ptr<Vehicle>> availableVehicles) {
    return std::make_shared<Station>(name, availableVehicles);
}

std::shared_ptr<Train>
DataAssembler::createTrain(int number, std::string departure, std::string destination, int departureHour,
                           int departureMinute, int arrivalHour, int arrivalMinute, int maxSpeed,
                           std::vector<int> vehicles) {

    auto departureTime = std::make_shared<Timer>(departureHour, departureMinute);
    auto arrivalTime = std::make_shared<Timer>(arrivalHour, arrivalMinute);
    auto train = std::make_shared<Train>(number, TrainStatus::NOT_ASSEMBLED, departure, destination, arrivalTime, departureTime, maxSpeed);

    train.get()->setVehicleList(vehicles);

    return train;
}

