#include "include/Vehicle.h"

int Vehicle::getId() const {
    return id;
}

Vehicle::Vehicle(int id, int type) : id(id), type(type) {}

int Vehicle::getType() const {
    return type;
}

const std::string Vehicle::getTypeString() const {
    std::string typeString;

    switch (type) {
        case 0:
            typeString = "Coach Car";
            break;
        case 1:
            typeString = "Sleeping Car";
            break;
        case 2:
            typeString = "Open Freight Car";
            break;
        case 3:
            typeString = "Covered Freight Car";
            break;
        case 4:
            typeString = "Electrical Engine";
            break;
        case 5:
            typeString = "Diesel Engine";
            break;
        default:break;
    }

    return typeString;
}


CoachCar::CoachCar(int id, int type, int numberOfChairs, bool availableWifi) : Vehicle(id, type), numberOfChairs(numberOfChairs), availableWifi(availableWifi) {
}

SleepingCar::SleepingCar(int id, int type, int numberOfBeds) : Vehicle(id, type),
                                                                              numberOfBeds(numberOfBeds) {}

OpenFreightCar::OpenFreightCar(int id, int type, int cargoCapacity, int floorArea) : Vehicle(id, type),
                                                                                                    cargoCapacity(
                                                                                                            cargoCapacity),
                                                                                                    floorArea(
                                                                                                            floorArea) {}

CoveredFreightCar::CoveredFreightCar(int id, int type, int cargoCapacity) : Vehicle(id, type),
                                                                                           cargoCapacity(
                                                                                                   cargoCapacity) {}

ElectricEngine::ElectricEngine(int id, int type, int maxSpeed, int power) : Vehicle(id, type),
                                                                                           maxSpeed(maxSpeed),
                                                                                           power(power) {}

DieselEngine::DieselEngine(int id, int type, int maxSpeed, int fuelConsumption) : Vehicle(id, type),
                                                                                                 maxSpeed(maxSpeed),
                                                                                                 fuelConsumption(
                                                                                                         fuelConsumption) {}
